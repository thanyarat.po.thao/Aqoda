//
//  AqodaTests.swift
//  AqodaTests
//
//  Created by Thanyarat Pothong Thaosriwich on 2019/11/14.
//  Copyright © 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import XCTest
@testable import Aqoda

class AqodaTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    func testGetInput() {
        let expectation = self.expectation(description: "Can't Get Input")
        let api = APIManager()
        var data: Any?

        api.getInput(fail: { (errorCode) in
             expectation.fulfill()
        }) { (repornData) in
            data = repornData
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 20)
        XCTAssertNotNil(data)
        
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
