//
//  HotelModels.swift
//  Aqoda
//
//  Created by Thanyarat Pothong Thaosriwich on 2019/11/14.
//  Copyright (c) 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import UIKit

enum ActionType: String {
    case created = "created"
    case booked = "booked"
    case cannotBook = "Cannot book"
    case checkout = "checkout"
    case cannotCheckout = "cannotCheckout"
    case roomNotavailable = "Room Not Available"
    case availableRooms = "availableRooms"
    case guestList = "guestList"
    case cannotBookFloor = "cannotBookFloor"
}

struct HotelModels {
    // Data struct sent to Interactor
    struct Request {
        var url: String = "https://gitlab.com/sittipong-codeapp/aqoda/raw/develop/input.txt"
    }
    
    // Data struct sent to Presenter
    struct DataResponse {
        var actions: [Action]!
    }
    struct Response {
        var action: ActionType!
        var data: Any?
        var option: Any?
    }
    
    // Data struct sent to ViewController
    struct ViewModel {
        struct Displayed {
            var string: String!
        }
    }
}
