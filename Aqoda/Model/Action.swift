//
//  File.swift
//  Aqoda
//
//  Created by Thanya on 14/11/19.
//  Copyright © 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import Foundation

struct Action {
    var key: String!
    var value: Any!
}
